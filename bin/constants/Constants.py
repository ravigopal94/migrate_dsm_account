"""
Constants.py
"""
AUTHORIZATION = "Authorization"
BEARER = "Bearer "
BASIC = "Basic "
ACCESS_TOKEN = "access_token"
ERROR = "error"
CONFIG_FILE= "config/migrate.json"

# app types
CERTIFICATE = "Certificate"
TRUSTEDCA = "TrustedCa"
AWSIAM = "AwsIam"
GOOGLESERVICEACCOUNT = "GoogleServiceAccount"
CREDENTIAL = "credential"
SECRET = "Secret"
JWT = "SignedJwt"

CERTIFICATE_L = "certificate"
TRUSTEDCA_L = "trustedca"
AWSIAM_L = "awsiam"
GOOGLESERVICEACCOUNT_L = "googleserviceaccount"
SECRET_L = "secret"
JWT_L = "signedjwt"

SLASH = "/"

# endpoints
SESSION_AUTH = "/sys/v1/session/auth"
GROUPS_API = "/sys/v1/groups"
APPS_API = "/sys/v1/apps"
ADMIN_APPS_API = "/sys/v1/apps?role=admin"
KEYS_API = "/crypto/v1/keys"
EXPORT_API = "/crypto/v1/keys/export"
PLUGINS_API = "/sys/v1/plugins"
USERS_API = "/sys/v1/users"
USERS_INVITE_API = "/sys/v1/users/invite"
ROLES_API = "/sys/v1/roles"
ACCOUNTS_API = "/sys/v1/accounts"
EXTERNAL_ROLES_API = "/sys/v1/external_roles"
# paths
LOGS_PATH = "logs/"

# dashes
SINGLE_DASH = "-" * 100

