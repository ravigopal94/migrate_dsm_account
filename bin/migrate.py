import queue
import copy
import sys
import threading
from concurrent.futures import ThreadPoolExecutor

from lib.general import read_config_file, set_logger, get_current_time_stamp, display_progress
from lib.rest_api import get_bearer_token, get_call, create_group, create_app, make_app_credential, create_user
from lib.rest_api import * #export_security_object, import_security_object, create_plugin, read_source_data
from constants import Constants
from collections import Counter
from operator import itemgetter

from urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

source_headers = None
target_headers = None
url = None

#lists for target group details
existing_target_names = []
existing_target_ids = []
patch_req_list = []
# dict for groups
source_groups = dict()
target_groups = dict()
existing_targetgrps = dict()
patch_req_dict = dict()
role_id_map = dict()
target_ldap_dict = dict()
source_ldap_dict = dict()
# counters
users_counter = Counter()
groups_counter = Counter()
apps_counter = Counter()
sobjects_counter = Counter()
plugins_counter = Counter()
group_roles_counter = Counter()
account_roles_counter = Counter()
admin_apps_counter = Counter()
external_roles_counter = Counter()
patch_groups_counter = Counter()

q_pool_size = 20
logger = None
log_file_name = None


def queue_execution(q, call_back, queue_size):
    while True:
        task = q.get()
        call_back(task)
        q.task_done()
        percent = (queue_size - q.qsize()) / queue_size
        percent = percent if percent > 0 else 0
        display_progress(percent)

def start_multithreaded(objects, call_back):
    q = queue.Queue()
    for i in range(q_pool_size):
        thread = threading.Thread(target=queue_execution, args=(q, call_back, len(objects),), daemon=True)
        thread.start()
    for obj in objects:
        q.put(obj)
    q.join()
    print("")


def parallel_execution(data, call_back_execution):
    """ executes creation of Groups/Apps/Source """
    with ThreadPoolExecutor(max_workers=20) as exe:
        exe.submit(call_back_execution, 2)
        exe.map(call_back_execution, data)


def create_target_plugin(plugin):
    """"""
    logger.debug("Plugin: " + plugin["name"])

    default_group_name = source_groups[plugin["default_group"]]["name"]
    default_group = target_groups[default_group_name]
    add_groups = []
    for group in plugin["groups"]:
        group_id = target_groups[source_groups[group]["name"]]
        add_groups.append(group_id)
    plugin_creation, sc = create_plugin(plugin, default_group, add_groups, target_headers, url)
    logger.debug(plugin["name"] + " " + str(sc))
    if sc == 201:
        logger.debug("Plugin: " + plugin["name"] + ", Status: " + "Success")
        plugins_counter["Number of plugins created"] += 1
    else:
        logger.error("Plugin: " + plugin["name"] + ", Status: " + "Failed")
        logger.error("Reason: " + plugin_creation[Constants.ERROR])
        if sc == 409:
            plugins_counter["Number of plugins already available"] += 1
        else:
            plugins_counter["Number of plugins failed to create"] += 1


def export_and_import_sobjs(sobj):
    """"""
    logger.debug("Exporting the security object: " + sobj["name"])
    export_sobj, sc = export_security_object(sobj["kid"], source_headers, source_url)
    if sc == 200:
        logger.debug("Exported the security object: " + export_sobj["name"])
        source_group_name = source_groups[sobj["group_id"]]["name"]
        target_group_id = target_groups[source_group_name]
        import_sobj, sc = import_security_object(export_sobj, target_group_id, target_headers, url)
        if sc == 201:
            logger.debug(sobj["name"] + " is migrated.")
            sobjects_counter["Number of sobjs created"] += 1
        else:
            logger.error(sobj["name"] + " is not migrated.")
            logger.error("Reason: " + import_sobj[Constants.ERROR])
            if sc == 409:
                sobjects_counter["Number of sobjs already available"] += 1
            else:
                sobjects_counter["Number of sobjs failed to create"] += 1
    else:
        logger.error("Exporting the security object failed: " + sobj["name"] + "\n" + "Reason: " + export_sobj[Constants.ERROR])
        sobjects_counter["Number of sobjs non-exportable"] += 1


def read_and_create_source_sobjs():
    is_sobjs_present = True
    offset = 0
    limit = 1000
    limit_str = "?limit={0}&offset={1}"
    logger.debug("Fetching all the security objects from source")
    while is_sobjs_present:
        limit_path = limit_str.format(str(limit), str(offset))
        logger.debug("Fetching security objects from " + str(offset) + " to " + str(offset + limit))
        get_source_sobjs, sc = get_call(source_url + Constants.KEYS_API + limit_path, source_headers)
        if sc == 200:
            if len(get_source_sobjs) == 0:
                is_sobjs_present = False
            else:
                limit_log = str(len(get_source_sobjs)) if len(get_source_sobjs) < limit else str(limit + offset)
                logger.debug("\nCreating security objects from " + str(offset) + " to " + limit_log)
                print("Creating security objects from " + str(offset) + " to " + limit_log)
                start_multithreaded(get_source_sobjs, export_and_import_sobjs)
                offset += 1000
        else:
            logger.error("Creating security objects from " + str(offset) + " to " + str(offset + limit) + " is failed.")
            logger.error("Reason: " + get_source_sobjs[Constants.ERROR])


def create_admin_app(app):
    credential = make_app_credential(app, source_headers, source_url)
    admin_app_creation, sc = create_app(app,None, None, credential, target_headers, target_url)
    if sc == 201:
        logger.debug("Admin app creation is successful: " + app["name"])
        admin_apps_counter["Number of admin apps created"] += 1
    else:
        logger.error("Failed to create an admin app: " + app["name"])
        logger.error("Reason: " + admin_app_creation[Constants.ERROR])
        if sc == 409:
            admin_apps_counter["Number of admin apps already available"] += 1
        else:
            admin_apps_counter["Number of admin apps failed to create"] += 1


def create_target_app(app):
    app_data, sc = get_call(source_url + Constants.APPS_API + Constants.SLASH + app["app_id"] + "?group_permissions=true",
                            source_headers)
    current_app_name = app["name"]
    if sc == 200:
        logger.debug("Fetching the app successful: " + current_app_name)
        default_group_name = source_groups[app["default_group"]]["name"]
        default_group = target_groups[default_group_name]
        add_groups = {}
        for group in app_data["groups"]:
            group_id = target_groups[source_groups[group]["name"]]
            add_groups[group_id] = app_data["groups"][group]
        credential = make_app_credential(app, source_headers, source_url)
        if Constants.ERROR not in credential:
            logger.debug("App credential fetched successfully: " + current_app_name)
            app_creation, sc = create_app(app_data, add_groups, default_group, credential, target_headers, url)
            if sc == 201:
                logger.debug("App creation is successful: " + current_app_name)
                apps_counter["Number of apps created"] += 1
            else:
                logger.error("Failed to create an app: " + current_app_name)
                logger.error("Reason: " + app_creation[Constants.ERROR])
                if sc == 409:
                    apps_counter["Number of apps already available"] += 1
                else:
                    apps_counter["Number of apps failed to create"] += 1
        else:
            logger.error("Failed to fetch the app credential: " + current_app_name)
            logger.error("Reason: " + credential[Constants.ERROR])
    else:
        logger.error("Failed to fetch the app: " + current_app_name)
        logger.error("Reason: " + app_data[Constants.ERROR])


def create_target_user(user):
    logger.debug("User Email Address: " + user["user_email"])
    user_creation, sc = create_user(user,url,target_headers)
    if sc == 201:
        logger.debug("User at address: " + user["user_email"] + ", Invitation status:" + " Success")
        users_counter["Number of users invited"] += 1
    else:
        logger.error("User at address: " + user["user_email"] + ", Invitation status:" + " Failed")
        if sc == 409:
            users_counter["Number of users already available"] += 1
        else:
            users_counter["Number of users failed to invite"] += 1
            logger.error("User failed to invite. Reason: " + user_creation[Constants.ERROR])


def patch_target_group(req_body):
    group_updation, sc = patch_group(req_body, url, target_headers)
    if sc == 200:
        logger.debug("Group: " + req_body["name"] + ", Status: Update Successful")
        patch_groups_counter["Number of groups updated to full state"] += 1
    else:
        logger.error("Group: " + req_body["name"] + ", Status: Update Unuccessful")
        logger.error("Group failed to patch. Reason: " + group_updation[Constants.ERROR])
        patch_groups_counter["Number of groups failed to be updated to full state"] += 1


def create_target_group(group):
    group_body = {
        "name": group["name"],
    }
    patch_body = dict()
    if "description" not in group.keys():
        group_body["description"] = ""
    else:
        group_body["description"] = group["description"]

    if "approval_policy" in group:
        del(((group)["approval_policy"]["protect_manage_operations"]))
        patch_body["approval_policy"] = group["approval_policy"]
    if "cryptographic_policy" in group:
        patch_body["cryptographic_policy"] = group["cryptographic_policy"]
    if "key_metadata_policy" in group:
        patch_body["key_metadata_policy"] = group["key_metadata_policy"]
    if "key_history_policy" in group:
        patch_body["key_history_policy"] = group["key_history_policy"]
    if "wrapping_key_name" in group:
        patch_body["wrapping_key_name"] = group["wrapping_key_name"]
    logger.debug("Group: " + group["name"])
    group_creation, sc = create_group(group_body, url, target_headers)
    logger.debug(group["name"] + " " + str(sc))
    if sc == 201:
        logger.debug("Group: " + group["name"] + ", Status: " + "Success")
        target_groups[group_creation["name"]] = group_creation["group_id"]
        groups_counter["Number of groups created"] += 1
        patch_req_dict[group_creation["group_id"]] = patch_body
        if len(patch_body) != 0:
            patch_body["group_id"] = group_creation["group_id"]
            patch_body["name"] = group_creation["name"]
            patch_req_list.append(patch_body)
    else:
        logger.error("Group: " + group["name"] + ", Status: " + "Failed")
        logger.error("Reason: " + group_creation[Constants.ERROR])
        if sc == 409:
            if len(patch_body) != 0:
                patch_body["group_id"] = target_groups[group["name"]]
                patch_body["name"] = group["name"]
                patch_req_list.append(patch_body)
            groups_counter["Number of groups already available"] += 1
        else:
            groups_counter["Number of groups failed to create"] += 1


def store_target_groups(groups, group_dict):
    for target_group in groups:
        group_dict[target_group["name"]] = target_group["group_id"]


def store_source_groups(groups, group_dict):
    for source_group in groups:
        group_dict[source_group["group_id"]] = source_group


def get_session(api_key, auth_type):
    logger.debug("Session initiated at " + auth_type + " for " + url)
    session, sc = get_bearer_token(api_key, url)
    if sc != 200:
        logger.error("Authentication failed at " + auth_type)
        sys.exit("Authentication failed at " + auth_type)
    headers = {Constants.AUTHORIZATION: Constants.BEARER + session[Constants.ACCESS_TOKEN]}
    logger.debug("Authentication successful.")
    print("Authentication successful at " + auth_type)

    return headers


def authenticate_nodes():
    # set logger
    global log_file_name
    log_file_name = Constants.LOGS_PATH + "migration_" + get_current_time_stamp() + ".log"
    global logger
    logger = set_logger(log_file_name)
    global url
    url = config["url"]
    # access the source node
    global source_headers
    source_headers = get_session(config["source_apikey"], "source")
    # access the target node
    global target_headers
    target_headers = get_session(config["target_apikey"], "target")


def segregate_roles(all_items):
    group_roles, account_roles = [],[]
    if len(all_items) == 0:
        return group_roles, account_roles
    for i in range(len(all_items)):
        if all_items[i]["kind"] == "group":
            group_roles.append(all_items[i])
        elif all_items[i]["kind"]  == "account":
            account_roles.append(all_items[i])
    return group_roles, account_roles


def create_target_roles(role):
    if "all_groups_role" in role["details"].keys():
        role["details"]["all_groups_role"] = role_id_map[role["details"]["all_groups_role"]]
    role_creation, sc = create_role(role, url, target_headers)
    if sc == 201:
        logger.debug("Custom " + role["kind"] + " role:" + role["name"] + ", Role ID at source: " + role["role_id"] + ", Status: Migrated")
        role_id_map[role["role_id"]] = role_creation["role_id"]
        if role["kind"] == "account":
            account_roles_counter["Number of custom " + role["kind"] + " roles created"] += 1        
        if role["kind"] == "group":
            group_roles_counter["Number of custom " + role["kind"] + " roles created"] += 1

    else:
        logger.error("Custom " + role["kind"] + " role:" + role["name"] + ", Role ID at source: " + role["role_id"] + ", Status: Migration Failed")
        logger.error("Reason: " + role_creation[Constants.ERROR])


def delete_target_roles(role):
    role_id = role["role_id"]
    role_deletion, sc = delete_role(role_id, url, target_headers)
    if sc == 204:
        logger.debug("Custom " + role["kind"] + " role:" + role["name"] + ", Role ID at target: " + role["role_id"] + ", Status: Deleted")
    else:
        logger.error("Custom " + role["kind"] + " role:" + role["name"] + ", Role ID at target: " + role["role_id"] + ", Status: Deletion Failed")
        logger.error("Reason: " + role_deletion[Constants.ERROR])


def authentication_handler(username, account_id, url):
    """Authenticates master node and non-master nodes"""
    print("User name: " + username)
    # authenticating master node
    auth_headers, sc = validate_credentials(username, account_id, url)
    if sc == 200:
        return auth_headers
    print("ERROR:" + url + ": " + auth_headers["error"])
    sys.exit(1)


def import_external_roles(external_role):
    source_id = target_ldap_dict[source_ldap_dict[external_role["source_id"]]]
    imported_ext_role, sc = import_ext_roles(source_id, external_role["name"], target_url, target_headers)
    if sc == 201:
        logger.debug("Imported external role: " + imported_ext_role["name"])
        external_roles_counter["Number of external roles imported"] += 1
    else:
        logger.error("Failed to import external role: " + external_role["name"])
        logger.error("Reason: " + imported_ext_role[Constants.ERROR])

def map_external_roles(external_role):
    if external_role["groups"] == {}:
        return
    mapped_groups, sc = map_groups(external_role, source_groups, target_groups, target_url, target_headers)
    if sc == 200:
        external_roles_counter["Number of external roles mapped"] += 1
        logger.debug("External role is successfully mapped: " + mapped_groups["name"])
    else:
        logger.error("External role failed to be mapped. Reason: " + mapped_groups[Constants.ERROR])


def patch_account_settings(settings, account_id, url, headers):
    patch_response, sc = patch_settings(settings, account_id, url, headers)
    if sc == 200:
        logger.debug("Account settings setup: Successful")
        print("Account settings setup: Successful")
    else:
        logger.error("Account settings setup: Unsuccessful. Reason: " + patch_response[Const.ERROR])
        print("Account settings setup: Unsuccessful")
    return patch_response

def store_ldap_dicts(source_acct_info, target_acct_info):
    global target_ldap_dict, source_ldap_dict
    for object in (source_acct_info["auth_config"]["ldap"]).keys():
        source_ldap_dict[object] = source_acct_info["auth_config"]["ldap"][object]["name"]
    for object in target_acct_info["auth_config"]["ldap"].keys():
        target_ldap_dict[object] = target_acct_info["auth_config"]["ldap"][object]["name"]
    target_ldap_dict = {v: k for k, v in target_ldap_dict.items()}


def init():    
    # set logger
    global log_file_name
    log_file_name = Constants.LOGS_PATH + "migration_" + get_current_time_stamp() + ".log"
    global logger
    logger = set_logger(log_file_name)
    global existing_target_names, existing_target_ids
    # authentication
    target_data = config["target"]
    source_data = config["source"]
    global source_url
    source_url = source_data["url"]
    global target_url
    target_url = target_data["url"]
    global url
    url = source_data["url"]
    # access the source node
    logger.debug("Session initiated at source for " + source_url)
    global source_headers
    source_headers = authentication_handler(source_data["username"], source_data["account_id"], source_url)
    logger.debug("Authentication successful.")
    # access the target node
    logger.debug("Session initiated at target for " + source_url)
    global target_headers
    target_headers = authentication_handler(target_data["username"], target_data["account_id"], target_url)
    logger.debug("Authentication successful.")    
    logger.debug("Migration initiated.")
    # patch settings
    admin_apps = read_source_data("Admin apps", Constants.ADMIN_APPS_API, logger, source_url, source_headers)
    print(Constants.SINGLE_DASH)
    print("Admin apps creation")
    start_multithreaded(admin_apps, create_admin_app)
    print(admin_apps_counter)
    # read, delete and rewrite custom roles (both account and group kinds)
    custom_target_roles = read_source_data("Target Custom Roles", Constants.ROLES_API, logger, target_url, target_headers)
    target_group_roles, target_account_roles = segregate_roles(custom_target_roles["items"])
    print(Constants.SINGLE_DASH)
    print("Clearing custom account roles at Target")
    start_multithreaded(target_account_roles, delete_target_roles)
    print(Constants.SINGLE_DASH)
    print("Clearing custom group roles at Target")
    start_multithreaded(target_group_roles, delete_target_roles)
    custom_source_roles = read_source_data("Source Custom Roles", Constants.ROLES_API, logger, source_url, source_headers)
    source_group_roles, source_account_roles = segregate_roles(custom_source_roles["items"])
    print(Constants.SINGLE_DASH)
    print("Group roles creation")
    start_multithreaded(source_group_roles, create_target_roles)
    print(group_roles_counter)
    print(Constants.SINGLE_DASH)
    print("Account roles creation")
    start_multithreaded(source_account_roles, create_target_roles)
    print(account_roles_counter)
    # read and write users
    users = read_source_data("users", Constants.USERS_API, logger, url, source_headers)
    print(Constants.SINGLE_DASH)
    print("Users creation")
    start_multithreaded(users, create_target_user)
    print(users_counter)
    source_acct_info = read_source_data("settings", Const.ACCOUNTS_API + Const.SLASH + source_data["account_id"], logger, source_url, source_headers)
    source_acct_info_temp = copy.deepcopy(source_acct_info)
    print(Const.SINGLE_DASH)
    print("Updating account settings")
    target_acct_info = patch_account_settings(source_acct_info_temp, target_data["account_id"], target_url, target_headers)
    store_ldap_dicts(source_acct_info, target_acct_info)
    # read source groups
    logger.debug("Getting all the groups from source account")
    print(Constants.SINGLE_DASH)
    print("Groups creation")
    grps = read_source_data("groups", Constants.GROUPS_API, logger, url, source_headers)
    store_source_groups(grps, source_groups) # id : group_body
    existing_targetgrps = read_source_data("groups", Constants.GROUPS_API, logger, url, target_headers)
    store_target_groups(existing_targetgrps, target_groups) # name : id
    # create groups
    start_multithreaded(grps, create_target_group)
    print(groups_counter)    
    # read external roles 
    ext_roles = read_source_data("External roles", Const.EXTERNAL_ROLES_API, logger, source_url, source_headers)
    print(Constants.SINGLE_DASH)
    print("Importing external roles")
    start_multithreaded(ext_roles, import_external_roles)
    print("Mapping external roles")
    start_multithreaded(ext_roles, map_external_roles)
    print(external_roles_counter)
    # read apps
    apps = read_source_data("apps", Constants.APPS_API, logger, url, source_headers)
    # create apps
    print(Constants.SINGLE_DASH)
    print("Apps creation")
    start_multithreaded(apps, create_target_app)
    print(apps_counter)
    # sobjs creation
    print(Constants.SINGLE_DASH)
    read_and_create_source_sobjs()
    print(sobjects_counter)
    # read plugins
    plugins = read_source_data("plugins", Constants.PLUGINS_API, logger, url, source_headers)
    # create plugins
    print(Constants.SINGLE_DASH)
    print("plugins creations")
    start_multithreaded(plugins, create_target_plugin)
    print(plugins_counter)
    # patch groups
    print(Constants.SINGLE_DASH)
    print("Modifying groups")
    start_multithreaded(patch_req_list, patch_target_group)
    print(patch_groups_counter)
    # print log file path
    print(Constants.SINGLE_DASH)
    print("\nPlease find the logs at: " + log_file_name + "\n")
# script starts
if __name__ == '__main__':
    config = read_config_file(Constants.CONFIG_FILE)
    init()
