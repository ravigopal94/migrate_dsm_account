import requests
from concurrent.futures import ThreadPoolExecutor

endPoint = "https://sdkms.fortanix.com"
#apiKey = "MzZmN2RlZDAtYjZlZi00ZGIyLWFiNWEtODQ1YmE3NzVlZWEyOlNuRGRKQXE3X0Zvb2JWZ1NxTGxMS1FLdWJoOHJmTGpVSHNXVGdDOXFYaUFIQXJQVkFlNW9uakVQbW5fcmhSSnI5VjBEZnYzZWRqQUlLQ0ZBWXRFeklB"
apiKey = "ODUxM2I4OTMtM2ZlZC00ZjFhLWJmMjEtM2MzNjE2YzQ3N2RkOlAyUGk2ejBLd0dUaG5lYVFiSzkyY0VxeFBzdHRkX2ZWT1BNeTMtVDZCLVllbjhyd0dLcmFYQVpnVk1yNVRIdDZ2dkVwN1pKSWpTNFB0cDVGakZvU0Nn"
bt = ""
group_prefix = "sobj_"


def get_bearer_token(apiKey, endPoint):
    """It will generate the bearer token """
    apiHeaders = {"Authorization": "Basic " + apiKey}
    response = requests.post(endPoint + "/sys/v1/session/auth", headers=apiHeaders)

    return response.json()["access_token"]


def create_sobj(sobj_name):
    """ """
    headers = {"Authorization": "Bearer " + bt}
    sobj_body = {"name": sobj_name,
                  "key_size": 192,
                  "obj_type": "AES",
                  "group_id": "33ee2233-01c4-463d-aa51-585064978cd9"}
    response = requests.post(endPoint + "/crypto/v1/keys", headers=headers, json=sobj_body)

    print("status: " + str(response.status_code) + " " + sobj_name)


def parallel_execution(data, call_back_execution):
    """ executes creation of Groups/Apps/Source """

    with ThreadPoolExecutor(max_workers=20) as exe:
        exe.submit(call_back_execution, 2)
        exe.map(call_back_execution, data)


bt = get_bearer_token(apiKey, endPoint)
sobj_names = []
for i in range(30):
    sobj_names.append(group_prefix + str(i))
    # create_group("g_" + str(i), endPoint, bt)
print(len(sobj_names))
parallel_execution(sobj_names, create_sobj)
