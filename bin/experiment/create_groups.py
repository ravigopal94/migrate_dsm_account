import requests
from concurrent.futures import ThreadPoolExecutor

endPoint = "https://sdkms.fortanix.com"
apiKey = "MzZmN2RlZDAtYjZlZi00ZGIyLWFiNWEtODQ1YmE3NzVlZWEyOlNuRGRKQXE3X0Zvb2JWZ1NxTGxMS1FLdWJoOHJmTGpVSHNXVGdDOXFYaUFIQXJQVkFlNW9uakVQbW5fcmhSSnI5VjBEZnYzZWRqQUlLQ0ZBWXRFeklB"
bt = ""
group_prefix = "g_"


def get_bearer_token(apiKey, endPoint):
    """It will generate the bearer token """
    apiHeaders = {"Authorization": "Basic " + apiKey}
    response = requests.post(endPoint + "/sys/v1/session/auth", headers=apiHeaders)

    return response.json()["access_token"]


def create_group(grp_name):
    """ """
    headers = {"Authorization": "Bearer " + bt}
    group_body = {"name": grp_name}
    response = requests.post(endPoint + "/sys/v1/groups", headers=headers, json=group_body)

    print("status: " + str(response.status_code) + " " + grp_name)


def parallel_execution(data, call_back_execution):
    """ executes creation of Groups/Apps/Source """

    with ThreadPoolExecutor(max_workers=20) as exe:
        exe.submit(call_back_execution, 2)
        exe.map(call_back_execution, data)


bt = get_bearer_token(apiKey, endPoint)
group_names = []
for i in range(500, 1000):
    group_names.append(group_prefix + str(i))
    # create_group("g_" + str(i), endPoint, bt)
print(len(group_names))
parallel_execution(group_names, create_group)
