# /usr/bin/python

import requests
import base64
import subprocess
import getpass
from requests.auth import HTTPBasicAuth
from constants import Constants as Const
from urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

def get_bearer_token(apikey, url):
    """It will generate the bearer token """
    headers = {Const.AUTHORIZATION: Const.BASIC + apikey}
    response = requests.post(url + Const.SESSION_AUTH, headers=headers)

    return get_response(response)


def extend_the_session(headers, url):
    response = requests.post(url + "/sys/v1/session/refresh", headers=headers)
    print(response)


def get_account_id(entity_id, headers, url):
    """It will get the account id"""
    response = requests.get(url + "/sys/v1/apps/" + entity_id, headers=headers)

    return response.json()['acct_id']


def get_call(url, headerss):
    """Any get call using bearer token header"""
    response = requests.get(url, headers=headerss)

    return get_response(response)


def export_security_object(kid, headers, url):
    """It will fetch the selected secret """
    export_body = {"kid": kid}
    response = requests.post(url + Const.EXPORT_API, headers=headers, json=export_body)

    return get_response(response)


def generate_result_log_string(old_name, new_name, status, padding_num):
    return old_name.ljust(padding_num, " ") + new_name.ljust(padding_num, " ") + status


def create_file_with_permissions(filename):
    subprocess.call(['touch', filename])
    subprocess.call(['chmod', '777', filename])


def patch_group(patch_body, url, headers):
    response = requests.patch(url + Const.GROUPS_API + Const.SLASH + patch_body["group_id"], json = patch_body, headers=headers)
    return get_response(response)


def create_group(group, url, headers):
    """ """
    group_body = group
    response = requests.post(url + Const.GROUPS_API, headers=headers, json=group_body)
    return get_response(response)


def create_user(user, url, headers):
 
    user_body = {
        "user_email": user["user_email"]
    }
    if "account_role" in user:
        user_body["account_role"] = user["account_role"]
    if "first_name" in user:
        user_body["first_name"] = user["first_name"]
    if "last_name" in user:
        user_body["last_name"] = user["last_name"]
    response = requests.post(url + Const.USERS_INVITE_API, headers=headers, json=user)
    return get_response(response)


def get_app_credential(app_id, headers, url):
    response = requests.get(url + Const.APPS_API + Const.SLASH + app_id + "/credential",
                            headers=headers)
    return get_response(response)


def make_app_credential(app, headers, url):
    credential = {}
    if app["auth_type"] == Const.SECRET:
        return credential
    response, sc = get_app_credential(app["app_id"], headers, url)
    if sc == 200:
        auth_type = app["auth_type"]
        if auth_type == Const.CERTIFICATE:
            credential[Const.CERTIFICATE_L] = response[Const.CREDENTIAL][Const.CERTIFICATE_L]
        elif auth_type == Const.TRUSTEDCA:
            credential[Const.TRUSTEDCA_L] = response[Const.CREDENTIAL][Const.TRUSTEDCA_L]
        elif auth_type == Const.GOOGLESERVICEACCOUNT:
            credential[Const.GOOGLESERVICEACCOUNT_L] = response[Const.CREDENTIAL][Const.GOOGLESERVICEACCOUNT_L]
        elif auth_type == Const.AWSIAM:
            credential[Const.AWSIAM_L] = response[Const.CREDENTIAL][Const.AWSIAM_L]
        elif auth_type == Const.JWT:
            credential[Const.JWT_L] = response[Const.CREDENTIAL][Const.JWT_L]
        return credential
    else:
        return response


def create_admin_apps(admin_app_info, url, headers):
    patch_body = {}

    if "name" in admin_app_info:
        patch_body["name"] = admin_app_info["name"]
    if "description" in admin_app_info:
        patch_body["description"] = admin_app_info["description"]
    if "app_type" in admin_app_info:
        patch_body["app_type"] = admin_app_info["app_type"]
    if "groups" in admin_app_info:
        patch_body["add_groups"] = admin_app_info["groups"]
    if "default_group" in admin_app_info:
        patch_body["default_group"] = admin_app_info["default_group"]
    if "interface" in admin_app_info:
        patch_body["interface"] = admin_app_info["interface"]
    if "ip_address_policy" in admin_app_info:
        patch_body["ip_address_policy"] = admin_app_info["ip_address_policy"]
    if "role" in admin_app_info:
        patch_body["role"] = admin_app_info["role"]
    if "secret_size" in admin_app_info:
        patch_body["secret_size"] = admin_app_info["secret_size"]
    if "oauth_config" in admin_app_info:
        patch_body["oauth_config"] = admin_app_info["oauth_config"]
    if "enabled" in admin_app_info:
        patch_body["enabled"] = admin_app_info["enabled"]
    if bool(patch_body):
        response = requests.post(url + Const.APPS_API, json = patch_body, headers=headers)
        
        return get_response(response)
    else:
        return "",""


def create_app(app, app_groups, default_group, credential, headers, url):
    app_body = {
        "name": app["name"]
    }
    if app_groups != None:
        app_body["add_groups"] = app_groups
    if default_group != None:
        app_body["default_group"] = default_group
    if "description" in app:
        app_body["description"] = app["description"]
    if "app_type" in app:
        app_body["app_type"] = app["app_type"]
    if "description" in app:
        app_body["description"] = app["description"]
    if "app_type" in app:
        app_body["app_type"] = app["app_type"]
    if "auth_type" in app:
        auth_type = app["auth_type"]
        app_body["auth_type"] = auth_type
        if auth_type != "Secret":
            app_body["credential"] = credential
    if "interface" in app:
        app_body["interface"] = app["interface"]
    if "oauth_config" in app:
        app_body["oauth_config"] = app["oauth_config"]
    if "secret_size" in app:
        app_body["secret_size"] = app["secret_size"]
    if "enabled" in app:
        app_body["enabled"] = app["enabled"]
    if "role" in app:
        app_body["role"] = app["role"]
    if "ip_address_policy" in app:
        app_body["ip_address_policy"] = app["ip_address_policy"]
    if "legacy_access" in app_body:
        app_body["legacy_access"] = app_body["legacy_access"]
    if "client_configurations" in app_body:
        app_body["client_configurations"] = app_body["client_configurations"]
    response = requests.post(url + Const.APPS_API, headers=headers, json=app_body)

    return get_response(response)


def import_security_object(sobj, target_group_id, headers, url):
    """ """
    sobject_body = {
        "name": sobj["name"],
        "obj_type": sobj["obj_type"],
        "group_id": target_group_id
    }
    if "description" in sobj:
        sobject_body["description"] = sobj["description"]
    else:
        sobject_body["description"] = ""

    if "value" in sobj:
        sobject_body["value"] = sobj["value"]
    if "elliptic_curve" in sobj:
        sobject_body["elliptic_curve"] = sobj["elliptic_curve"]
    if "fpe" in sobj:
        sobject_body["fpe"] = sobj["fpe"]
    if "aes" in sobj:
        sobject_body["aes"] = sobj["aes"]
    if "transient" in sobj:
        sobject_body["transient"] = sobj["transient"]
    if "key_size" in sobj:
        sobject_body["key_size"] = sobj["key_size"]
    if "rsa" in sobj and sobj["rsa"] is not None:
        del sobj["rsa"]["key_size"]
        sobject_body["rsa"] = sobj["rsa"]
    if "pub_key" in sobj:
        sobject_body["pub_key"] = sobj["pub_key"]
    if "custom_metadata" in sobj:
        sobject_body["custom_metadata"] = sobj["custom_metadata"]
    if "deactivation_date" in sobj:
        sobject_body["deactivation_date"] = sobj["deactivation_date"]
    if "enabled" in sobj:
        sobject_body["enabled"] = sobj["enabled"]
    if "kcv" in sobj:
        sobject_body["kcv"] = sobj["kcv"]
    if "key_ops" in sobj:
        sobject_body["key_ops"] = sobj["key_ops"]
    if "seed" in sobj:
        sobject_body["seed"] = sobj["seed"]
    if "aria" in sobj:
        sobject_body["aria"] = sobj["aria"]

    response = requests.put(url + Const.KEYS_API,
                            headers=headers,
                            json=sobject_body)

    return get_response(response)


def delete_security_object(kid, headers, url):
    """delete the security object based on the given data."""
    response = requests.delete(url + Const.KEYS_API + Const.SLASH + kid, headers=headers)

    return get_response(response)


def create_plugin(plugin, default_group, groups, headers, url):
    plugin_body = {
        "name": plugin["name"],
        "default_group": default_group,
        "add_groups": groups,
        "plugin_type": plugin["plugin_type"],
        "source": plugin["source"],
        "enabled": plugin["enabled"]
    }
    response = requests.post(url + Const.PLUGINS_API, headers=headers, json=plugin_body)

    return get_response(response)


def create_role(role, url, headers):
    role_body = {
        "name": role["name"],
        "kind": role["kind"],
        "details": role["details"]
    }
    if "description" in role.keys():
        role_body["description"] = role["description"]
    else:
        role_body["description"] = []
    response = requests.post(url + Const.ROLES_API, headers = headers, json = role_body)

    return get_response(response)
    

def delete_role(role_id, url, headers):
    response = requests.delete(url = url + Const.ROLES_API + Const.SLASH + role_id, headers=headers)

    return get_response(response)


def read_source_data(data_type, api, logger, url, headers):
    logger.debug("Fetching all the " + data_type + " from source.")
    get_source_data, sc = get_call(url + api, headers)
    if sc == 200:
        logger.debug("Fetched all the " + data_type + " from source.")
        logger.debug("Total number of " + data_type + " in source: " + str(len(get_source_data)))
    else:
        logger.error("Failed to fetch the " + data_type + ".")
        logger.error("Reason: " + get_source_data[Const.ERROR])
    return get_source_data


def get_response(response):
    result = {}
    status_code = response.status_code
    if 200 <= status_code < 205:
        if status_code != 204:
            result = response.json()
    else:
        result["error"] = response.text
    return result, status_code


def authenticate(username, password, url):
    """It will generate bearer token """
    api_headers = HTTPBasicAuth(username, password)
    response = requests.post(url + "/sys/v1/session/auth", auth=api_headers)

    return get_response(response)


def validate_credentials(user_name, account_id, url):
    """Validates the credentials"""
    # authentication
    password = getpass.getpass("Enter the password of " + url + ":")
    # password = ""

    authentication, sc = authenticate(user_name, password, url)
    if sc == 200:
        auth_headers = {"Authorization": "Bearer " + authentication['access_token']}
        account_selection, sc = select_account(account_id, auth_headers, url)
        if sc == 200:
            return auth_headers, sc
        else:
            return account_selection, sc
    else:
        return authentication, sc
    

def select_account(account_id, headers, url):
    """Select the account"""
    body = {"acct_id": account_id}
    response = requests.post(url + "/sys/v1/session/select_account", headers=headers, json=body)#, verify=False)
    return get_response(response)


def get_settings(acct_id, url, headers):
    response = requests.get(url + Const.ACCOUNTS_API + Const.SLASH + acct_id, headers=headers)

    return get_response(response)


def get_admin_apps(url, headers):
    response = requests.get(url + Const.APPS_API + "?role=admin", headers=headers)

    return get_response(response)

    
def clear_ldap(account_id, url, headers):
    patch_body = {}
    target_settings, sc = get_settings(account_id, url = url, headers = headers)
    if "auth_config" in target_settings:
        if "ldap" in target_settings["auth_config"]:
            if bool(target_settings["auth_config"]["ldap"]):
                somelist = list(target_settings["auth_config"]["ldap"].keys())
                patch_body["del_ldap"] = somelist
    response = requests.patch(url + Const.ACCOUNTS_API + Const.SLASH + account_id, json = patch_body, headers=headers)
    return get_response(response)


def patch_settings(settings_info, account_id, url, headers):
    clear_ldap(account_id, url, headers)

    patch_body = {}
    
    if "description" in settings_info:
        patch_body["description"] = settings_info["description"]
    if "approval_policy" in settings_info:
        patch_body["approval_policy"] = settings_info["approval_policy"]
    if "organization" in settings_info:
        patch_body["organization"] = settings_info["organization"]
    if "country" in settings_info:
        patch_body["country"] = settings_info["country"]
    if "phone" in settings_info:
        patch_body["phone"] = settings_info["phone"]
    if "notification_pref" in settings_info:
        patch_body["notification_pref"] = settings_info["notification_pref"]
    if "auth_config" in settings_info:
        patch_body["auth_config"] = settings_info["auth_config"]
        if "ldap" in settings_info["auth_config"]:
            if bool(settings_info["auth_config"]["ldap"]):
                somelist = list(settings_info["auth_config"]["ldap"].values())
                # patch_body["auth_config"]["add_ldap"] = somelist
                patch_body["add_ldap"] = somelist
                del(patch_body["auth_config"]["ldap"])
    if "auth_type" in settings_info:
        patch_body["auth_type"] = settings_info["auth_type"]
    if "client_configurations" in settings_info:
        patch_body["client_configurations"] = settings_info["client_configurations"]
    if "logging_configs" in settings_info:
        if bool(settings_info["logging_configs"]):
            patch_body["add_logging_configs"] = list(settings_info["logging_configs"].values())
    if "custom_metadata" in settings_info:
        patch_body["custom_metadata"] = settings_info["custom_metadata"]
    if "key_undo_policy" in settings_info:
        patch_body["key_undo_policy"] = settings_info["key_undo_policy"]
    if "cryptographic_policy" in settings_info:
        patch_body["cryptographic_policy"] = settings_info["cryptographic_policy"]
    response = requests.patch(url + Const.ACCOUNTS_API + Const.SLASH + account_id, json = patch_body, headers=headers)

    return get_response(response)


def import_ext_roles(source_id, name, url, headers):
    body = {
            "source_id": source_id,
            "name": name
            }
    response = requests.post(url + Const.EXTERNAL_ROLES_API, headers=headers, json = body)
    return get_response(response)


def map_groups(ext_role_data, source_groups, target_groups, url, headers):
    add_groups = dict()
    for i in range(len(ext_role_data["groups"])):
        source_group_id = list(ext_role_data["groups"].keys())[i]
        target_group_id = target_groups[source_groups[source_group_id]["name"]]
        add_groups[target_group_id] = list(ext_role_data["groups"].values())[i]
    patch_body = {
        "mod_groups" : {},
        "add_groups" : add_groups
    }
    response = requests.patch(url + Const.EXTERNAL_ROLES_API + Const.SLASH + ext_role_data["external_role_id"], headers = headers, json = patch_body)
    return get_response(response)
