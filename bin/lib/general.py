from datetime import datetime
import json
import base64
import logging
import sys


def read_config_file(file_path):
    """reads the config file"""
    config = None
    with open(file_path, 'r') as openfile:
        config = json.load(openfile)

    return config


def make_api_key(app_id, cred_obj):
    secret_byte_str = app_id + ":" + cred_obj["credential"]["secret"]
    api_key = base64.b64encode(secret_byte_str.encode("ascii"))

    return api_key.decode('UTF-8')


def set_logger(file):
    """logger initialization"""
    logging.basicConfig(filename=file,
                        format='%(asctime)s - %(levelname)s - %(message)s',
                        filemode='w')
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    return logger


def get_current_time_stamp():
    now = datetime.now()
    date_time = now.strftime("%m-%d-%Y-%H-%M-%S")
    return date_time


def display_progress(percent):
    bar_length = 20
    progressed = '█' * int(round(percent * bar_length))
    in_progress = '-' * (bar_length - len(progressed))
    print(("\rPercent: [{0}] {1}%".format(progressed + in_progress, (int(round(percent * 100))))), end="", flush=True,
          file=sys.stdout)
