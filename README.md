**About**

* This project is to migrate the dsm data from one account to another account

**Pre-requisites**

* Fortanix DSM access for atleast two accounts
* Authentication method: username/password
* To migrate any security object, it should be exported
* Python 3 or above should be installed
* config/migrate.json should be configured

**config file**

* It has 6 parameters

* url: url in "source" should refer to source node dsm url, url in "target" should refer to target node dsm url.
* username: username in "source" refers to source node username, username in "target" refers to target node username
* account_id: account_id in "source" refers to account id of source node, account_id in "target" refers to account_id in target node


* Please refer the below json data. It will be at config/migrate.json.

```
{
  "source": {
    "url": "<source node dsm url>",
    "username": "<target node username>",
    "account_id": "<source node account ID>"
  },
  "target": {
    "url": "<target node dsm url>",
    "username": "<target node username>",
    "account_id": "<target node account ID>"
  }
}
```

**How to execute**

* Create and empty folder named "logs" in migrate_dsm_account/bin if it is not already present.
* Edit the config/migrate.json as instructed above in the file.
* Change directory to migrate_dsm_account/bin and run the command:
```
python3 migrate.py
```
* You will be asked to enter password for source and target nodes.
* After you enter, script will give a console output similar to examples/example_console_output.txt
* You can check the logs of the script in the logs folder.

**Algorithm**

* Read config/migrate.json
* Authenticate source and target nodes
* Read admin apps from source and create them in target node
* Delete all the custom account roles and custom group roles in target node
* Read custom account roles, custom group roles from source and create them in target node
* Read users from the source and invite them in target node
* Read account settings from source and update the account settings in the target node
* Read the groups from source and create them in target node
* Read external roles from source and import them in targte node
* Map external roles to groups in target 
* Read the apps from source and create them in target node
* Read the security objects from source and create them in target node
* Read plugins from source and create them in target node
* Update the groups at target to full working state
* Update the log file in logs folder throughout the runtime